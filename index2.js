var express = require('express');
var router = express.Router();
var dateacs = new Date();
//var acs = "PRO";
var tbot = require('./bot');
var maxdelta = 1000*600;     //10 min time access




/* Route for access request */
router.get('/try', function(req, res, next) {
	var ndate = new Date();
	var acs = tbot.getacs();
  	var delta = ndate.getTime()-dateacs.getTime();
  	//console.log(delta);

  	if((delta>maxdelta)){
  		tbot.setacs("PRO");
		acs = tbot.getacs();
  		dateacs = ndate;
  	}

  	//send message to admin about request
  	tbot.notiacs('ACCESS: '+acs+' time: '+ndate);
  	
  	//generate response
  	res.json({
  		access: acs,
      	time: dateacs
  	});
});

/*GET ACCESS*/
router.get('/acs', function(req, res, next) {
	var acs;
	acs = tbot.getacs();
  	res.json({
    	access: acs,
    	time: dateacs
  	});

  	//if(acs == "OK")
  	//	acs = "PRO";
});



/* GET ERROR*/
/*router.get('/err', function(req, res, next) {
    throw new Error('oops');
});*/

router.use( function (err, request, response, next) {
  console.log(err);
  response.status(500).send('Wtf broke!');
});

module.exports = router;

